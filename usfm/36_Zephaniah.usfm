\id ZEP
\h Sofonia
\toc1 Buku ya Profeta Sofonia
\toc2 Sofonia
\toc3 Sof
\mt1 Buku ya Profeta Sofonia
\imt Boyingisi
\is1 Mokomi
\ip Sofonia azalaki mobotama wa Yuda, ekoki kozala mpenza Yeruzalem, oyo mpe makomi mazali kotalisa mosala mwa ye bo profeta. Bomoto bwa ye, ndenge elakisami na ba nkoma ya ye, ebimisi bosembo bwa ye, alingaka lokuta te mpe elikya ya ye se na mosala mwa Nzambe, oyo azali nkolo ya bileko. Lolenge ye azali koloba na babola, na bato ya mawa elakisi bolingo bwa ye ona bato bosawa. Boniboni amipesaki na Nzambe eyebani mpenza te. Kasi eyano ya ye na mbe­la ya Nzambe apesaki yango ntembe te se na mbala yoko mpe na bonsomi bonso. Atikalaki kopusana pembeni ya baprofeta basusu te (3,4).
\is Eleko 
\ip Makanisi ma Sofonia mpo na mambi ya eleko ya ye mazali polele mpenza mpe ekomonisa ’te ayebi ut’o kala lingomba lya bayangeli ba ekolo, oyo epesi nzela ya koyeba eleko ya mosala mwa ye. Tokoki kondima ya solo ’te mosala mwa ye esalemaki o eleko ya lingomba ike ya Josia (640-630). Eleko eye eulani na maye makomami na buku etali bato to engumba ya Yeruzalem. Bapaleli profeta atako eza­li mpe mpo ya Eloi etali mingi mpe­nza baye bazali nzinga nzinga ya ye (1,8). Eleko ya Sofonia ezali yambo mpenza eye ya bokasi bwa Asuria, na minyokoli mpe bobomi na yango : kufa ya bikolo katikati ya Efrata na mbu Mediterani, bobebisi Damasko o mobu 732, bozwi Samaria o mobu 722 mpe bokei bwa bato baye o boombu, bokwei bwa Tyro o mobu 701 mpe bopanzi Sidoni libandeli tii suka o mobu 671, bobebisi Thebe o mobu 663, na Babilon o mobu 689 … 
\is Nsango 
\ip Mokolo mwa Yawe. Likanisi liye lya mokolo mwa Yawe ebotami o tango eye mboka ezali na mobulu, kimia te mpe minyoko mpenza mpo na bato bayebi lika­mbo te. Mokolo mwa Yawe ezali ta­ngo oyo ye akobeta baye bazali konyo­kolo ekolo ya ye mpe akobikisa bango o maboko ma banguna baye. Lokola eleko ya bobimi, akozongela makamwisi ma nsomo on’a bikolo. Soki bato babosawa bakolikya kobika, mokolo mwa Yawe mokotikala mokolo mwa mawa enene kaka mpo ya bato na bikolo te kasi mpe mpo na ekelamu nyonso ezali kobikela na mokili. Nzokande, mokolo mwa Yawe elekisa­mi mpenza te epayi ya Sofonia lokola nsuka ya molongo to ya bileko, kasi lokola mbongwana mpe etonga ya sika ya ekolo ya Nzambe, nsuka ya ekolo ya masumu. Mpe manso makosuka na nzembo ya esengo enene mpo na baye bake batikali. Makebisi on’a Yeruzalem mpe on’a bikolo profeta atii Yeruzalem o esambiselo ya Nzambe mpe alakeli ye mabe, pa­mba te mboka yango ezali na loboko te, moto magbongi, zambi bakonzi, bazuzi, baprofeta na mpe banganga Nzambe na yango balakisi moto makasi na lolaka la Nzambe. Banso bakozwa etumbu : bato banene na bake, bakambi ekolo, basali ya leta, bato ya mombongo to na baye bazangi boyambi. Mpo na bikolo bya bapagano, bango mpe bakozwa etumbu. Profeta asakoli ’te bikolo bina bikopa­nzana mpe bikosila. Na mimeseno ya baprofeta ya koyeba ntina ya bilembo oyo ezali kolakisama o ekolo ya ye, Sofonia azali mobandisi ya ba nkoma eye ezali kolobela nsuka ya molongo.
\mt1 Yuda o mokolo mwa Yawe *
\c 1
\p
\v 1 Maloba Yawe alobaki na Sofonia, mwana wa Kus, nkoko wa Gedalya, oyo azali mwana wa Ama­rya, mwana wa Kiskia, o nta­ngo Yozia, mwana wa Amon azalaki mokonzi wa Yuda.
\v 2 Yawe alobi boye : Solo, nakolongola biloko binso o mokili.
\v 3 Nakolongola bato na nyama, nakolongola ndeke ya likolo na mbisi ya mai, nakolongola bisimbisi bikweisaki bato, nakolongola bato, basila o mokili. Maloba ma Yawe.
\v 4 Nakotombola loboko mpo ya kosala Yuda mabe na mpe bato banso ba Yeruzalem *. Nakolongola o esika ena maka­mbo ma Baal, basali na banganga nzambe ba ye,
\v 5 baye bakokumbamelaka o mito­ndo mya ndako minzoto mya likolo *, baye bakokumbamelaka Yawe mpe bakolayaka ndai na nkombo ya Milkom *,
\v 6 baye bakopesaka Yawe mokongo, bakolukaka mpe bakotunaka ye te.
\v 7 Banso bazala nye o boso bwa Mokonzi Yawe, mpo mokolo mwa Yawe mokomi penepene ! Yawe alengeli limpati lisantu na moboma, asantisi baye babyangemi.
\v 8 Mokolo Yawe akolamba moboma mwa ye, akopesa bayangeli mpe bankumu etumbu, na baye banso bakolataka bilamba bya bikolo bisusu.
\v 9 Mokolo mona nakotumbola baye banso bakoyingelaka o te­mpelo ya nzambe mosusu *, mpe bakotondisaka ndako ya mokonzi wa bango na mosolo bazwi na bitumba mpe na bokosi.
\v 10 Yawe alobi boye : Mokolo mona bokimeli bokoyokana o ‘Ezibeli ya Mbisi’, boleli o ‘Mboka ya Sika’ *, mpe lokito lonene o ngomba.
\v 11 Bino bato ba Esika ya Bipolu, bolela, mpo bakangi zando ya Kanana, bato banso bakotangaka mosolo bakufi.
\v 12 Mokolo mona nakosimba mwi­nda o maboko mpo ’te natala Yeruzalem malamu o bisika bya molili. Nakotumbola bato bazali lokola vino ebebi, baye bamilobeli o mitema : « Yawe akoki kosala malamu te, mabe mpe te. »
\v 13 Mokolo mona bakobotolo bango nkita mpe bakobuka ndako ya bango ; bakotonga ndako, kasi bakofanda te, bakolona nzete ya vino, kasi bakomele vino te.
\v 14 Mokolo mwa Yawe mobelemi, mokolo monene, mozali koya mbangu mpenza ! O mokolo mwa Yawe bato bakolela makasi *, ata elombe akobelela basalisi ba ye.
\v 15 Mokolo mona, mokolo mwa nkanda ! Mokolo mwa malozi mpe mwa bobangi, mokolo mwa mpasi mpe mwa mawa, mokolo mwa molili mpe mwa butu boindo tu, mokolo likolo likoinda na ma­mpata manene,
\v 16 mokolo bakobete ebeleli ya bitu­mba na mondule, mpo babundisa bato ba bibombamelo mpe ba manongi.
\v 17 Nakoyokisa bato kwokoso, bakotambola lokola bato ba lola­nda, zambi basali masumu o miso ma Yawe. Makila ma bango makosopama lo­kola mai, misopo mya bango mikobwakema lokola bipoli.
\v 18 Palata na wolo ya bango bikoki kobikisa bango te, mokolo mwa nkanda ya Yawe. Móto mwa zuwa lya ye mokosilisa mokili mobimba ; akobebisa mpe akoboma bato banso o mokili.
\c 2
\s1 Bato babongola mitema
\p
\v 1 Bino bato ba ekolo bozanga nsoni, bokanisa mpe bosangana,
\v 2 mpo ’te bapupola bino te lokola bakopupolaka matiti, nkanda enene ya Yawe ekwela bino te, awa mokolo mwa nkanda ya Yawe mokoki naino te.
\v 3 Babola * banso ba nse baluka Yawe, bino baye bokotosaka mibeko mya ye. Bozala na motema bosembo mpe na bosawa, mbele bokobika o mokolo mwa nkanda ya ye.
\v 4 Solo, Gaza ekokoma esika ya mpa­mba, Askalon ekokoma mopotu, bakobengana bato ba Asdod na moi mpenza, mpe bakopikola Ekron.
\v 5 Mawa na bino bato penepene na mbu, bino bandeko ba ekolo ya Kereti *, Yawe alobi mpo ya bino : « Yo Kanana, mokili mwa ba-Filisti, nakoboma yo na bato ba yo ba­nso. »
\v 6 Mokili mwa mbu mokokoma esobe enene, lopango la mpata la bakengeli ba bibwele.
\v 7 Mokili moye mokozala mwa bato ba Yuda baye batikali, bakoleisa mpata ya bango o mabelé mana ; na mpokwa bakopema o ndako ya Askalon, mpo Yawe Nzambe wa bango ako­ya kosalisa bango, akoya kobongisa makambo ma bango.
\v 8 Nayoki mafinga ma bato ba Moab mpe ba Amon ntango bazalaki kofinga bato ba ngai mpe kofunda na mabelé ma ba­ngo.
\v 9 Yawe wa bokasi bonso Nzambe wa Israel alobi : « Ya solo, nalayi o nkombo ya ngai : Moab ekokoma lokola Sodoma, mpe Amon lokola Gomora ; mabelé ma bango makotonda na nzube, makokoma libulu lya mongwa, mopotu seko. Bato ba ngai baye bakotikala o mokili bakobotolo bango biloko, mwana mboka oyo akotikala ako­zwa mabelé ma bango. »
\v 10 Tala etumbu bato ba bikolo bakozwa mpo ya lolendo, mpo bafingaki mpe bafundaki o miso ma bato ba ekolo ya Yawe wa bokasi.
\v 11 Yawe akoyokisa bango nsomo, akokweisa bikeko binso bya ba­nzambe ba bango. Bato ba bisanga binso bakoku­mbamela ye, moko moko o mokili mwa ye.
\v 12 Nalobi mpe na bino, bato ba Etiópia : « Mopanga mwa ngai mokotubola bino. »
\v 13 Akotumbola bato ba ekolo ya Nordi, akoboma Asur * mobimba, akokomisa engumba Ninive mopotu, ekokoma zelo lokola eliki.
\v 14 Ngombe ikoya kolala wana, na nyama isusu ndenge na ndenge : esulungutu na yanganga ikolala wana na butu, penepene na maninisa nzembo ya ndeke ekoyokana, mpo nzete inso ikatani.
\v 15 Liboso ezalaki engumba ya bise­ngo, ya kimia ; bazalaki komilobela : « Nani akokani na biso ? » Ekomi eloko ya nsomo sikawa, mpe nganda ya nyama. Bato banso bakoleka wana bakoseke mpe bakolakisa yango na mosapi.
\c 3
\s1 Maloba ona bayangeli ba ekolo
\p
\v 1 Mawa na motomboki, banyokoli ba bato bakomi na mbindo !
\v 2 Yawe abengaki bango, bandimi te ; apalelaki bango, bayoki te ; batii mitema na Yawe te ; balingi kobeleme na Nzambe wa bango te.
\v 3 Bayangeli ba bango bazali lokola nkosi ya nkanda, bazuzi ba bango lokola nkoi o esobe, ikobombaka biloko tee lobi na ntongo te.
\v 4 Baprofeta bazali na maloba ma mpamba mpe na mayele ma bokosi, banganga Nzambe bakosantolaka biloko bisantu, bakobebisaka mibeko mya Nza­mbe.
\v 5 Kasi Yawe azali wana, azali na bosembo, akosalaka mabe soki te. Mokolo na mokolo akoyebisaka mibeko mya ye, etani ntongo ye azali se wana. Kasi moto azangi bosembo akoyokaka nsoni te.
\v 6 Nabomi bikolo bisusu, nabomi manongi ma bango, nabebisi balabala, bongo bato bakoleka wana lisusu te ! Bingumba bibebi, bato bazali wana lisusu te.
\v 7 Namilobelaki ’te : « Mbele okobanga ngai *, mbele okoyoka liteya mpe okobosana makebisi ma ngai te. » Nzokande noki bakobi kosala mabe !
\v 8 Yango wana Yawe alobi boye : Bozila mokolo bokoyeba ngai, mokolo nakoteleme mpo ya kofu­nda bino. Nasili nakani kosangisa bato ba bikolo binso *, nakoyokela bango nkanda, nakokitisela bango nkele ya ngai, mokili mobimba mokosila na móto mwa zuwa lya ngai.
\s1 Bapagano bakumisa Nzambe wa solo
\p
\v 9 Nakopetola mbebu ya bato ba bikolo binso mpo ’te banso babelela nkombo ya Yawe, mpe basalela ye na motema moko.
\v 10 Bato bakouta leka bibale bya Etiópia, esika bapanzani, mpo ya kosalela ngai mpe kobonzela ngai mabonza.
\v 11 Mokolo mona okoyoka nsoni lisusu te mpo ya mabe mingi osalaki ngai, zambi nakolongola bato ba monoko monene o mboka yo, mpe okotika kofunda o ngomba ya ngai esantu.
\v 12 Nakotika epai ya yo se ndambo ya bato, bato ba bosawa, baye bazali na lolendo te *, baye bakotia elikya na nkombo ya Yawe.
\v 13 Bato bakotikala o Israel bakozanga bosembo lisusu te, bakobuka lokuta lisusu te ; bakokosa lisusu te, kasi bakolia mpe bakopema na boboto, moto moko akotungisa bango lisusu te.
\v 14 Bato ba Sion, boganga na esengo ! Bato ba Israel, boyemba nzembo ya nsai ! Bato ba Yeruzalem, bosepela mpe boyoka mitema nsai !
\v 15 Yawe alongoli etumbu ya yo, apanzi banguna ba yo ; Yawe, Mokonzi wa Israel, azali na yo, obanga makambo ma mpasi lisusu te.
\v 16 Mokolo mona bakoloba na Yeruzalem : « Obanga te, Sion, maboko ma yo malembe te !
\v 17 Yawe Nzambe wa yo azali na yo, ye elombe mobikisi ! Akoyoka nsai enene mpo ya yo, akolakisa bolingi bwa ye lisusu *, mpe akobinela yo na migango mya esengo,
\v 18 lokola o mikolo mya eyenga. Nalongoli malozi epai ya yo, bakosambwisa yo lisusu te.
\v 19 Tala boniboni nakosala banyokoli ba yo banso. O ntango ena nakobikisa baye babukani makolo, nakosangisa baye babungami. Nakosala ’te bakumisa yo, nkombo ya yo eyebana o bikolo biye bazalaki koyoka nsoni.
\v 20 O ntango ena nakokamba yo, nakosangisa bato ba yo banso ; nakosala ’te bakumisa yo, nkombo ya yo eyebana o nse mobimba, nakobongisa makambo ma yo manso o miso ma yo. Maloba ma Yawe. »
