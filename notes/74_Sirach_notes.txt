(1).	O maloba maye ma liboso mokomi ayebisi ntina nini nkoko wa ye akomaki buku eye o ebrei, mpe eloko nini etindi ye abongola yango o greki. Asali yango o mobu mwa 138 liboso lya Yezu Kristu. Toyebi nkombo ya ye te ; nkombo ya nkoko wa ye ekomami o 50,27.
1,1.	Ekomami mpe o Mos 1,7 ; 2,6 ’te bwanya bonso bouti na Nzambe. Biso bakristu toyebi ’te Yezu Kristu moto ayeli bato bwanya bwa Nzambe, leka bato banso (1 Kor 24-30).
1,4.	Milongo 5 na 7 mizali te. Totiki yango na ntina. Batiaki (sekulo 5 ileki) mitango mya biteni mpe mya milongo o buku eye ezalaki na maloba mingi (mbala mingi makambo makomamaki mbala ibale), leka buku eye ; maloba mana makomami awa lisusu te, mita­ngo mya milongo myango mpe te.
1,11.	Tala Biy 1,7+
1,26.	Bobele moto atosi mibeko mya Nzambe azali moto wa bwanya bwa solo. Na botosi Nzambe moto akolisi bwanya bwa ye.
2,1.	O eleko buku eye ekomami ba-Yuda bazalaki konyokwama mpo baboyaki kolanda mimeseno mya ba-Greki (bapagano), mpe balingaki se kotosa Nzambe wa solo se moko. Se bongo na moi mwa lelo : bakristu bakoki kotungisama mpe konyokwama mpo ya botosi Nsango Elamu ya Yezu Kristu (Mt 5,11).
6,28.	O Mt 11,29 tokoki kotanga nde­nge Yezu, oyo azali na bwanya bwa solo, akokisi maloba maye.
7,14.	Tala Mt 6,7.
7,15.	Ebongi ’te moto akomipesaka mingi na boyekoli Mobeko mpe mateya ma bwanya asala mpe mosala mwa maboko. Kasi lokola ekomami o 38, 24-34, moto akomipesaka bobele na mosala mwa maboko akoki kokolisa bwanya bwa ye te, ata misala mya ye mikobongelaka bato mingi.
7,21.	O Lv 25,39-52 mpe o Mbk 15,12-18 ekomami ndenge moombo wa ba-Ebrei akoki komisikola o Mobu mwa Sabato (Lisabwi). Nzokande ba­nkolo basusu bazalaki kotosa mibeko miye na motema monso te.
7,24.	Lilako liye lya Ben-Sira lizali lya solo : babokolo bana na bolingi, kasi balakisa bango mpe botosi ; moto akondimelaka mwana wa ye ntango inso, akobebisa motema mwa ye.
7,33.	Ut’o kala ba-Israel bazalaki ko­kunda bawei na lokumu mpenza (2 Sam 21,10-14 ; Yer 22,19 ; Tob 1,17-18 ; 12,12) ; kasi bobele o ntango ya ba­ Makabe babandi kobonza mabonza mpe kosambela mpo ya bango (2 Mak 12, 38-46).
7,36.	O mokolo mwa liwa lya moto, Nzambe akozongisela ye engebene na maye asalaki (11,26-28). O eleko Ben-Sira akomi buku eye bato bakomi koyeba ’te Nzambe akopesa bato mbano to etumbu nsima ya liwa lya bango.
8,10.	Tolendisa mposa ya motema mwa mosumuki te, mpo topelisa móto mwa nkanda ya ye te.
9,13.	Moto abuti lopango la engu­mba o ntango ya etumba akoki ko­kweya o nse, kasi akoki mpe kozoka na makula ma banguna.
10,13.	O Biblia ndakisa ya bitumbu biye bikoki kokwela bato ba lolendo ipesami mingi : linongi lya Babel (Lib 11,1-9), bitumbu bikwelaki ba-Ezipeti (Bob 7-11), ndenge basoda ba Senekarib basuki (2 Bak 19,35). Tokanisa mpe bitumbu biye bikwelaki basumuki basusu : maye makwelaki bato ba Sodoma na Gomora (Lib 19) mpe o ntango ya mpela enene (Lib 6-9).
10,19.	Na bonsomi bonso moto akoki kopono : akumisa Nzambe to aboya kotosa ye.
11,19.	Mbele Yezu akanisaki maye makomami awa ntango alobaki moulani mwa moto wa nkita oyo azangi mayele (Lk 12,19).
12,4.	O eteni eye (12,4-7) emononi polele ’te Ben-Sira ayebaki naino mateya ma Nsango Elamu te, maye matindi biso tosala banguna mpe malamu (Lk 6,27) ; ayebaki mpe buku ya Biyele (25,21-22) te : otia makala ma móto o motó mwa mo­nguna, mpo ’te akoka kobongola ezalela ya ye.
12,11.	Se lokola bakoki kolongola bogugi o talatala ya mbengi noki, lokuta la monguna lokomonono noki, soko azwi nzela ya kosala mabe.
13,24.	Bato ba nkita banso bazali bato babe te, babola banso bazali bato balamu te : kozala na nkita ezali na makama, kozala bobola ezali mpe na makama. Manso matali se motema mwa moto moko moko (Biy 30,8).
15,11.	Mabe mauti wapi ? Epai ya Nzambe te ! Mpo lisumu lya bato likoki ko­zwela ye litomba lyoko te. Kasi moto akoki kopono : kosala mabe to malamu. Soko atosi ye na bonsomi boye moto alakisi Nzambe ’te andimi ye na motema moko. Mbala esusu Nza­mbe asalaki ’te mabe ma bato mabota mbuma ilamu ; tokanisa maye makwelaki Yozefu o Ezipeti (Lib 50,20). Tala mpe Rom 7,7-24 ; Yak 1,13-15.
16,7.	Tala Lib 6,1-7.
16,8.	Sodoma : tala Lib 19,1-29.
16,9.	Baye bazalaki o Kanana ntango ba-Israel bayei kofanda wana.
16,14.	(15 Nzambe akomisi Farao motó makasi mpo ’te andima te, bato banso bayeba bongo bikela bya ye. 16 Bato banso bakoki koyeba ngolu ya ye, alakisi bango mwinda mpe nkembo ya ye.)
16,29.	Ben-Sira alandi awa molongo mwa maye makomami o Lib 1.
17,8.	Milongo 5 na 9 mizali te : tala 1,5+
17,23.	Ben-Sira ayebi solo ’te Nza­mbe akopesa bato banso mbano, to liboso to nsima ya liwa.
18,21.	Ba-Yuda bakanisaki ’te bato bazalaki koyoka mpasi mpo ya masumu ma bango. Yezu ateyaki ’te ezali bongo te (Yo 9,1-3).
18,24.	Mbele balakisi awa mokolo mwa liwa lya moto, nsuka ya molóngó te.
18,27.	Moto wa bwanya akoki koboya lisenginya.
21,8.	Akosengela kofuta nyongo ya ye ; yango ekoki kotungisa ye mingi, mbele akokufa liboso ’te elaka ya ye ekoka.
21,27.	Oyo akosala lisumu ezali Satana te, kasi moto, mpo alandi mposa ya motema mwa ye.
23,10.	Mpo akokisi elako ya ye te, to amemyi Nkombo ya Nzambe te. Lisumu lileki linene : kolaya ndai ya lokuta (23,11).
24,1.	Se lokola Nzambe, bwanya bozali bipai binso, mpe bokobotela bato banso bayambi bwango mbuma mingi.
24,25.	O eteni eye Ben-Sira alakisi biso bibale bya Eden, biye bikosalaka ’te mabelé ma ekolo ena mabota mbuma mingi : mbuma iye, nde mbuma ya bwanya. Na bibale bitangemi o Lib 2,10 mokomi abakisi Yordane na Nil, biye ba-Yuda bayebaki malamu.
25,24.	Awa batangi lisumu lya bato ba yambo ; o Lib 3,6 mpe Rom 5,12 ekomami ’te mobali asalaki se mabe lokola mwasi.
25,26.	Mobeko mwa Moze mozalaki kolingisa koboma libala (Mbk 24,1-4), kasi Yezu ayei kobongisa libala lokola Nzambe akelaki lyango : likoki kokufa te (Mt 19,3-9 ; Ef 5,31).
30,7.	Tata akokanga mpota iye mwana azoki. Bakoki mpe kobongola boye : akokanga mpota ya ye moko.
30,21.	Motema nsai mokobongela mpe bokolongonu bwa moto. Bakristu bakanisa awa mpe maloba ma S. Polo : ‘Bosepelaka o Mokonzi ntango inso’ (Fil 4,4), mpe maye ma Yezu : ‘Nalobi na bino bongo mpo ’te esengo ya ngai ezala na bino’ (Yo 17,13).
31,21.	O ntango ena moto abyangi baninga o limpati abondeli bango mi­ngi balia mingi mpenza ; mbala esusu balekisaki bongo ndelo na bolei (Est 1,8).
33,7.	Mokomi amituni ndenge nini bobele bato basusu bakolongaka mbala mingi o makambo ma bango. Mbele ekoki kozala mpo Nzambe akokabelaka moto na moto makabo ma ye, engebene na bosembo bwa ye.
33,16.	Ben-Sira ayebi ’te bato basusu ba bwanya basili bakomi minkanda liboso lya ye ; mbele, na boyekoli bikoma bya bango abuki mbuma, leka ba­ngo. Tala mpe 24,30-34.
33,25.	O ntango ena bankolo base­ngelaki kotosa mibeko mitali baombo, mpo baombo mpe bazalaki na makoki (Bob 21,1-27 ; Lv 25,46 ; Mbk 15,12-18). Bobele mpo ya Yezu Kristu bakosala makasi mpo ya kopesa bango bo­nsomi (Gal 3,28 ; Ef 6,9 ; Kol 4,1 ; Flm).
34,6.	Ben-Sira ayebi ’te Nzambe akoki koyebisa bato makambo na ndo­to (Lib 20,3-7 ; 8,10-17 ; 37,5-11 ; Baz 7,13), kasi ayebi mpe ’te bato bakoki kobunga. Kolanda Mobeko mwa Nza­mbe eleki malamu.
34,18.	Mokomi alandi awa mateya ma baprofeta : losambo losengeli kouta na motema mwa moto, bobele na monoko mwa ye te. Bobele moto wa bosembo akoki kobonzela Nzambe libonza bo ebongi. Yango wana akomi ’te moto atosi mibeko aleki oyo abo­nzi mabonza.
34,20.	Ben-Sira apaleli baye bakobotoloko babola biloko, mpo bazali lokola babomi : Nzambe, oyo azali tata wa bango, akoki kondima ’te baboma bango bongo ?
36,1.	O eleko ya Ben-Sira, mibu soki 190 liboso lya Y.K., ba-Greki bazalaki kotinda ba-Yuda ’te batosa mimeseno mya bango. O eteni eye tokoki kotanga losambo la ba-Yuda basengi ’te etumbu ekwela bato ba bikolo bisusu mpo ’te ba-Israel bakoka kosambela bo ebongi na bango ; na nsuka tokotanga losambo mpo ’te bato ba bikolo bisusu bakoka kobongola mitema, bongo basambela Nzambe se moko.
36,24.	Tala Lib 2,18.
38,1.	Ba-Yuda basusu bakanisaki ’te moto wa boyambi akoki koluka lisalisi lya monganga te (2 Mkl 16,12), kasi Ben-Sira ayebisi bango ’te babungi, mpo mo­nganga akokangaka mpota, kasi bobele Nzambe moto akobikisaka.
38,5.	Tala Bob 15,23-25.
38,16.	Ben-Sira alingi mimeseno mya ekolo ya ye ; yango wana asengi batangi ba ye ’te batosa milulu mya bokundi bawei, kasi alingi te ’te balekisa ndelo ya maye matindami.
38,24.	O 38,24-34 Ben-Sira alakisi misala mya maboko ndenge na ndenge (moto wa bilanga, mokabinda, motuli na mosali mbeki) ; kasi moto moko te wa misala miye minso akotikalaka na ntango ya koyekola bwanya malamu. Ye moto aponomi mpo ayekola Mobeko amoni ’te mosala mwa ye moleki misala minso na bolamu (39,1-11). Tala mpe 7,15.
39,1.	Mpo akoma na bwanya bwa solo moteyi wa Mobeko asengeli koyekola Minkanda Misantu, bikoma bya bato ba bwanya ba kala, kokutana na bato mingi o mibembo mya ye, mpe kosambela (39,1-5).
39,23.	Bato basusu bakanisaki ’te mai ma Mbu ya Mongwa (Mbu ya Liwa) mokomaki bololo o ntango Sodoma na Gomora ibebaki (Lib 19,24-26).
42,15.	Eteni ya liboso ya buku eto­ndi na biyele mpe na mateya ma bwa­nya. O eteni ya bibale eye ebandi awa bakoyembela bwanya na bonene bwa Nzambe, lokola mamononi o bikelamo bya ye binso (42,15 - 43,33) mpe lokola alakisi na ndenge akambaki bato ba ekolo ya ye na bato banene baye abimisaki (44,1 - 50,24).
43,7.	Biyenga binene bibale bya ba-Yuda, Pasika na Eyenga ya Biema, bibandaki o butu bwa mokolo mwa sa­nza ya sika (o mokolo mwa 14 to mwa 15 ya sanza, na ndenge biso tokotangaka).
44,1.	Ben-Sira akomanyola bikela bya bato banene ba Israel ; akoyebisa ntina nini akokumisa moko moko wa bango. Tokoki kotanga bikela bya Enok, Nowe, Abarama, Izaka na Yakob o buku Libandela ; biye bya Moze na Arone mingi o buku Bobimi. Bikela bya bankoko basusu bikomami o Yozue, Bazuzi, buku ibale ya Samuel, Esdra na Neemia.
45,23.	Pinekas (Mit 25,7-13) oyo azalaki moto malamu solo, kasi mokomi apusi kokumisa ye leka bato basusu baye basalaki mpe malamu.
46,7.	Mokomi akumisi mpe Kaleb koleka (Mit 14,6-10 ; Yoz 14,10-14).
50,1.	Simon azalaki nganga mokonzi o mibu mya 220-195 liboso lya Y.K. o Tempelo ya Yeruzalem. Mbele Ben-­Sira ayebaki Simon oyo azalaki mpe moka­­mbi wa engumba ; akumisi ye mi­ngi mpo ya misala mya ye mya bole­ndisi mpe bobo­ngisi Yeruzalem. O 50, 11-21 tokoki kotanga ndenge nga­nga mokonzi oyo ayangeli milulu mya losambo.
50,26.	Banguna baye basato ba Israel : ba-Seir (Edom), ba-Filisti na ba-­Samaria ; toyebi te ntina nini bakomi likambo liye awa.
51,6.	Awa bafundi Ben-Sira na makambo ma lokuta epai ya mokonzi, etikalaki moke bakatela ye etumbu ya liwa, kasi mpo ya bobateli bwa Nza­mbe emononi ’te azalaki na likambo lyoko te.