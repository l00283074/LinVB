1,3.	Bakangaki mokonzi oyo wa Yuda, Yekonia, o boombo (2 Bak 24,8-17 ; Yer 22,24-30).
1,7.	Ata Tempelo etumbami, bato bakobi kosambela penepene na esika ya yango.
1,8.	Mbele Yoyakim akangemaki o boombo te ; ye moto, ata azalaki nga­nga mokonzi te, azalaki kokamba milulu mya losambo.
3,9.	O eteni eye ya ibale ya buku bakumisi bwanya (3,9 - 4,4) : bobele Nzambe azali na bwanya bwa solo ; na makasi ma bango bato bakoki kokoma na bwanya te.
4,1.	Moto alingi kokoma na bwanya bwa solo asengeli kotosa mibeko miye Nzambe ayebisi na monoko mwa Moze. O ntango ya Baruk mibeko mingi misili mikomami o buku.
4,30.	O ebrei nkombo Yeruzalem lokola ’te ‘engumba ya boboto’ : Nzambe akopesa engumba eye boboto boye alakeli bato ba ye (5,4).
5,9.	Monkanda moye mokomami na profeta Yeremia, ata ekotangemaka na nkombo ya ye ; toyebi mokomi mwa mwango te. Kasi toyebi ’te ekomami mibu mingi nsima ya Yeremia, ntango ba-Yuda basilaki kokoma mibu mo­nkama na ndambo o Babilon. O mo­nkanda moye balakisi bompamba bwa bikeko ; bikoki ata komibatela te.
6,14.	Molongo moye mokomami mbala mingi awa (22.28.39.44.51.56) : totosa mpe tomemya Nzambe se moko.