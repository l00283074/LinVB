1,1.	S. Polo abandi monkanda moye na maloba makasi. O monkanda moye mobimba akumisi ba-Galata ata na likambo lyoko te.
1,10.	Lokola liboso, ntango azalaki konyokolo bakristu.
1,16.	Ekomami : epai ya nsuni na makila te.
1,17.	Arabia : ekolo o Sudi ya Damasko, bokonzi bwa ba-Nabateo.
1,19.	Yakobo, ndeko wa Mokonzi (tala Mt 12,46). Moyangeli oyo wa Eklezya ya Yeruzalem azalaki moko wa bapostolo te ; Bik 15,13 ; 21,18.
2,1.	Barnaba oyo atangemi mpe o Bik 9,27 ; 11,22-25 ; 13 - 15 ; 1 Kor 9,6.
2,7.	Ekomami : baye bakatisaki nzoto te, baye bazali ba-Yuda te.
2,9.	Ekomami : baye bazali makonzi manene o Eklezya.
2,11.	O Bik 11-15 tokoki kotanga mambi ma Eklezya ya Antiokia na mpe mosala Polo asalaki kuna.
2,12.	Polo andimi ezalela eye ya Petro te, mpo yango ekokaki kokabola bakristu. Polo alobi polele ’te baye bakatisi nzoto baleki bakristu basusu, baye bakatisi nzoto te, na bolamu te.
2,15.	Polo atangi bapagano awa na bokinoli te lokola ba-Yuda mingi bazalaki kosala : se likambo lyoko lizali na ntina mpenza : koyamba Yezu, ozala mo-Yuda to moto wa ekolo esusu.
3,3.	Makambo ma bokatisi nzoto na maye manso ba-Yuda bazalaki kosala engebene na Mobeko (Tala mpe Fil 3, 3...).
3,6.	Nzambe andimi Abarama liboso mpenza mpo ya boyambi : tala Lib 15,6 ; Rom 4,3.
3,20.	Mobembisi akosalelaka bato baike, kasi Nzambe azali se moko.
3,27.	Ekomami : Bolati Kristu (na boyambi mpe na batisimo).
4,3.	Makambo ma nse eye : Bapagano bazalaki kokanisa ’te banzambe bakoyangelaka mokili mpe makambo ma bato.
4,12.	Mpo amikomisa lokola ba-Galata, Polo atiki kolanda mimeseno mya ba-Yuda ; sikawa akebisi ba-Galata basusu, baye bakomi kondimela ba-Yuda balingi kondimisa bango mimeseno mya bango ; soko basali bongo, bakokabwana na ye (1 Kor 9,21-22).
4,17.	Bato basusu : Basakoli ba-Yuda o Galata.
5,6.	Moto wa boyambi bwa solo akoki kozanga bolingi te.
5,24.	Basili babaki nzoto ya bango o kuruse na bokuli batisimo. 
6,2.	Mobeko mwa Kristu : Mobeko mwa bolingani.
6,11.	Polo ameseni kotangela moto maloba ma ye, akoma mango. Lokola tokoki kotanga mpe o Rom 16,22 azalaki kokoma maloba ma nsuka ma monkanda na loboko la ye moko na nkoma inene.
6,17.	Bilembo bya Yezu : Mpota iye bazokisi Polo mpo ya Kristu.