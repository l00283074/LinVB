1,1-2.	–Tala Rom 1,1-7.
	–Silvano : Tala 1 Tes 1,1.
	–Timoteo : Tala 1 Kor 4,17.
1,8.	–Móto mokopela makasi : Elakisi nguya mpe nkembo nta bonene ya Nzámbe.
1,10.	–Mokolo : Tala 1 Tes 5,2.
2,3.	Mpo etali likambo liye bakoki mpe kotanga Mt 24,24 ; 1 Tim 4,1 ; 2 Pe 3,3 ; Boy 13. Satana akobunda etumba ya ye ya nsuka na Nzambe mpe na Eklezya na maboko ma moto moko, oyo santu Polo atangi awa na nkombo isato : ‘Oyo abó­yá Nzámbe’, Oyo ako­bungama seko, Monguna. Nsima ya Polo, Eklezya akotanga ye Monguna wa Kristu (Anti-Kristu). Tala mpe 1 Yo 2,18.22 ; 4,3 ; 2 Yo 7, esika Yoane alakisi ye lokola moto akomekola bomimonisi, boyei mpe maka­mwisi ma Kristu.
2,6.	Mpo ezali kopekisa Monguna wa Kristu kobima sikawa, ba-Tesaloniki bakoki koyeba yango, zambi Polo asili alimboleli bango. Kasi biso toyebi yango te.
3,1.	Mokristu akoki kosala eloko te, soko Nzambe asalisi ye te ; Polo aku­ndoleli ba-Tesaloniki ’te bakozwa lisalisi lya Nzambe na losambo. Losambo la bango lokolendisa bango o nzela ya boyambi.
3,6.	Bato ba goigoi : Bakoki mpe kobongola : ‘bato ba mobulu’.