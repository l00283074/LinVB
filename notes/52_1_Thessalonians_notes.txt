1,1.	–Silvano : Ezali bobele Silas oyo ata­ngemi o Bik 15-18 ; 2 Kor 1,19 ; 1 Pe 5,12.
	–Timoteo : Tala 1 Kor 4,17.
1,5.	Nzambe azalaki kolendisa na makamwisi mateya ma Nsango Elamu maye santu Polo azalaki koteya bato ba Tesaloniki.
1,6.	Liloba : Nsango Elamu ya libiki Kristu ayeli biso.
1,7.	–Masedonia ezali o ndelo ya Nordi ya Akaya.
	–Akaya ezali eteni enene ya ekolo ya Greke ya lelo.
1,10.	–Bakristu bakozilaka Boyei bwa Mokonzi na elikya enene. Tala Lk 21,28 : ‘Botombola mitó !’
	–‘Nkanda ekoya’ : o mokolo Kristu akokata makambo ma bato banso (tala Mt 3,7 : Lk 3,7).
2,2.	Polo anyokwami mingi o Filipi, yango wana akendeki noki o Tesaloniki (Bik 16,16-17 ; Fil 1,30).
2,5.	Maloba ma boleti : maloba ma sukali.
2,7.	Polo akokaki kosenge ba-­Tesaloniki lisalisi o maye masengeli na ye, mpo azalaki apostolo. Kasi asengi bango eloko te, mpo alingaki te ’te lisalisi lya bango limonono lokola lifuta lya mosala mwa kosangela Nsango Elamu (2,9 ; 1 Kor 9,1-18).
2,14.	Ut’o ebandela bakristu ba Yudea banyokwami mingi (Bik 8,13 ; Gal 1,13). Ezala o Yudea to o Tesaloniki, bakristu baye bapiki mpende o ntango ya minyoko, balakisi ’te Liloba lya Nzambe lizali kosala mosala solo.
2,16.	–Tala Lib 15,16 ; Mt 23,32.
	–To : … esili ekweli bango seko.
2,19.	–Tala Ez 16,12 ; 23,42.
	–Mokolo Mokonzi akoya : Tala 1 Kor 1,7-8 ; 1 Tes 4,13-18.
3,5.	Satana oyo akosenginyaka bakristu mpo ’te batika koyamba mpe bazongela makambo ma bapagano.
3,13.	–Tala 5,23 ; 1 Kor 1,8 ; 15,23 ; Za 14,5.
	–‘Basantu’ : Awa alakisi banzelu : tala Mt 16,27 : 25,31 ; 2 Tes 1,7.10.
4,4.	To nzoto ya moto moko moko (5,23 ; Rom 12,1 ; 1 Kor 6,19), to nzoto ya bolongani (1 Pe 3,7).
4,11.	Bato ba goigoi bakolobaka : « Nsuka ya molongo ekomi penepene ! » Polo ateyi : Mokristu asala mosala na molende !
4,12.	Ekomami ‘bato ba libanda’.
4,13.	–O Tesaloniki Polo ateyaki mingi Boyei bwa Mokonzi (1,10 ; 2 Tes 2,1). Mbele alobaki eloko te mpo ya maye makokwela bandeko baye bakowa liboso lya Boyei bwa Mokonzi, bongo bandeko basusu bakomi motema likolo mpo ya liwa lya baye bawaki o mikolo mina.
	–‘Bato basusu’ : baye bayambi te.
4,17.	–O mampata : Tala Dan 7,13 ; Mt 26,64.
	–Elongo na Mokonzi : Tala 5,10 ; 2 Kor 5,8 ; Fil 1,23.
5,1.	Ntango mpe ngonga ya Boyei bwa Mokonzi eyebani bobele na Nzambe : Tala Bik 1,7.
5,2.	Mokolo Mokonzi akoya (to : Mokolo mwa Mokonzi) : Mokolo Kris­tu akoya na nkembo o nsuka ya molongo (Tala mpe 1 Kor 1,8).
5,20.	Santu Polo akebisi ba-Tesaloniki : Batala soko makambo manso mauti so­lo na Elimo Santu (tala mpe 1 Kor 12,1).