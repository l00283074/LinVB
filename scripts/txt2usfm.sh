#!/bin/bash
#Pour supprimer les bom des fichiers utf8
find . -type f -exec sed 's/\xEF\xBB\xBF//g' -i.bak {} \; -exec rm {}.bak \;
# pour chaque fichier dans ls *.sfm
#for FILE in `ls *.txt` ; do
#Commence par mettre à la ligne chaque verset
find . -name "*.txt" -exec sed -i 's/\xe2\x80\x82/ /g' {} \;
find . -name "*.txt" -exec sed -i 's/\s[0-9][0-9][0-9]\s/\n&/g' {} \;
find . -name "*.txt" -exec sed -i 's/\s[0-9][0-9]\s/\n&/g' {} \;
find . -name "*.txt" -exec sed -i 's/\s[0-9]\s/\n&/g' {} \;
find . -name "*.txt" -exec sed -i 's/(^[0-9])\t(1)/\1\n\2/g' {} \;
find . -name "*.txt" -exec sed -i 's/(^[0-9][0-9])\t(1)/\1\n\2/g' {} \;
find . -name "*.txt" -exec sed -i 's/(^[0-9][0-9][0-9])\t(1)/\1\n\2/g' {} \;
find . -name "*.txt" -exec sed -i 's/^[ ]*//g' {} \;
find . -name "*.txt" -exec sed -i 's/^[\t]1/1/g' {} \;
find . -name "*.txt" -exec sed -i '/./!d; 1i\\\id \n\\mt1 ' {} \;
find . -name "*.txt" -exec sed -i 's/ \°/\n\\v /g' {} \;

for FILE in `ls *.txt` ; do
#Commence par mettre à la ligne chaque verset
  #sed -i 's/^\s//g' $FILE
 # sed -i 's/\t1/\n1\n/g' $FILE
 # sed -i 's/ [0-9][0-9] /\n&/g' $FILE
  #sed -i 's/\s[0-9] /\n&/g' $FILE
#Supprime les espaces éventuel en début de ligne
  #sed -i 's/^[ ]*//g' $FILE
#Supprime les lignes vide et ajoute "\id \mt1" à la première ligne
  #sed -i '/./!d; 1i\\\id \n\\mt1 ' $FILE
#Colle la ligne 2 et 3
  #sed -i '/\\mt1$/N; s/\n/ /' $FILE

#Ajout "\v " devant chaque ligne commençant par un puis deux chiffres suivit d'un espace et de texte
  sed -i 's/^[0-9]\s[a-z]*/\\v &/g' $FILE
  sed -i 's/^[0-9][0-9]\s[a-z]*/\\v &/g' $FILE
  sed -i 's/^[0-9][0-9][0-9]\s[a-z]*/\\v &/g' $FILE
#ajoute \c devant chaque ligne commencant par deux chiffre
  sed -i 's/^[0-9]$/\\c &\n\\p/g' $FILE
  sed -i 's/^[0-9][0-9]$/\\c &\n\\p/g' $FILE
  sed -i 's/^[0-9][0-9][0-9]$/\\c &\n\\p/g' $FILE
  #rename 's/txt/sfm/g' $FILE Commande à utiliser si vous voulez travailler avec des fichier .txt
  # Mettre \s1 devant les noms des livres bibliques
 sed -i 's/^[A-Z]/\\s1 &/g' $FILE
 sed -i 's/^[1-2] [A-Z]/\\s1 &/g' $FILE
done
#Fusion avec autre script attention ici usfm pas txt
#Commence par mettre à la ligne chaque verset
find . -name "*.usfm" -exec sed -ri 's/([0-9][0-9])([a-z])/\n\\v \1 \2/g' {} \;
find . -name "*.usfm" -exec sed -ri 's/([0-9])([a-z])/\n\\v \1 \2/g' {} \;
find . -name "*.usfm" -exec sed -ri 's/([0-9][0-9])(«)/\n\\v \1 \2/g' {} \;
find . -name "*.usfm" -exec sed -i 's/([0-9])(«)/\n\\v \1 \2/g' {} \;
